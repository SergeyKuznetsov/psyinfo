import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:psyinfo/main.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  var colorFilter =
      MaterialStateProperty.all<Color>(Color(0xFF35D4BE).withOpacity(0.5));
  final color_1 =
      MaterialStateProperty.all<Color>(Color(0xFF35D4BE).withOpacity(0.5));
  final color_2 = MaterialStateProperty.all<Color>(Color(0xFF35D4BE));

  final _loginController = TextEditingController();
  final _passwordController = TextEditingController();

  @override
  void dispose() {
    _loginController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFFEFEFE),
      body: Form(
        child: ListView(
          children: [
            SizedBox(
              height: 106.0,
            ),
            Align(
              alignment: Alignment.center,
              child: Container(
                height: 194,
                width: 244,
                child: Column(
                  children: [
                    Align(
                      alignment: Alignment.centerRight,
                      child: Text(
                        'Psy',
                        style: TextStyle(
                            fontSize: 128,
                            color: Color(0xFF35D4BE),
                            fontWeight: FontWeight.w800,
                            fontFamily: 'Montserrat-ExtraBold.ttf'),
                      ),
                    ),
                    Align(
                      alignment: Alignment.centerRight,
                      child: Text(
                        'info',
                        style: TextStyle(
                            fontSize: 48,
                            height: 0.68,
                            color: Color(0xFF000000),
                            fontWeight: FontWeight.w800,
                            fontFamily: 'Montserrat-ExtraBold.ttf'),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 52.0, left: 20.0, right: 18.0),
              height: 54.0,
              child: TextFormField(
                controller: _loginController,
                onChanged: (text) {
                  if (text.isNotEmpty) {
                    setState(() {
                      colorFilter = color_2;
                    });
                  } else if (text.isEmpty) {
                    setState(() {
                      colorFilter = color_1;
                    });
                  }
                },
                style: TextStyle(
                    fontSize: 16,
                    color: Color(0xFF101B2B),
                    fontWeight: FontWeight.w500,
                    fontFamily: 'Montserrat-Medium.ttf'),
                decoration: InputDecoration(
                  hintText: 'Логин',
                  hintStyle: TextStyle(
                      fontSize: 16,
                      color: Color(0xFF101B2B).withOpacity(0.5),
                      fontWeight: FontWeight.w500,
                      fontFamily: 'Montserrat-Medium.ttf'),
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(8.0)),
                    borderSide:
                        BorderSide(color: Color(0xFF35D4BE), width: 2.0),
                  ),
                  focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(8.0)),
                      borderSide:
                          BorderSide(color: Color(0xFF35D4BE), width: 2.0)),
                ),
                keyboardType: TextInputType.phone,
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 8.0, left: 20.0, right: 18.0),
              height: 54.0,
              child: TextFormField(
                controller: _passwordController,
                obscureText: true,
                style: TextStyle(
                    fontSize: 16,
                    color: Color(0xFF101B2B),
                    fontWeight: FontWeight.w500,
                    fontFamily: 'Montserrat-Medium.ttf'),
                decoration: InputDecoration(
                  hintText: 'Пароль',
                  hintStyle: TextStyle(
                      fontSize: 16,
                      color: Color(0xFF101B2B).withOpacity(0.5),
                      fontWeight: FontWeight.w500,
                      fontFamily: 'Montserrat-Medium.ttf'),
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(8.0)),
                    borderSide:
                        BorderSide(color: Color(0xFF35D4BE), width: 2.0),
                  ),
                  focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(8.0)),
                      borderSide:
                          BorderSide(color: Color(0xFF35D4BE), width: 2.0)),
                ),
                keyboardType: TextInputType.phone,
              ),
            ),
            Container(
              margin: EdgeInsets.only(
                  top: 52.0, left: 16.0, right: 16.0, bottom: 24.0),
              height: 52.0,
              width: double.infinity,
              child: ElevatedButton(
                onPressed: _submitForm,
                child: Text(
                  'Войти',
                  style: TextStyle(
                      fontSize: 16,
                      color: Color(0xFFFEFEFE),
                      fontWeight: FontWeight.w700,
                      fontFamily: 'Montserrat-Bold.ttf'),
                ),
                style: ButtonStyle(backgroundColor: colorFilter),
              ),
            ),
          ],
        ),
      ),
    );
  }

  _submitForm() {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (BuildContext context) => PsyInfo(),
      ),
    );
  }
}
