import 'package:flutter/material.dart';

class PsyInfo extends StatefulWidget {
  @override
  _PsyInfoState createState() => _PsyInfoState();
}

class _PsyInfoState extends State<PsyInfo> {
  int selectedIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFE5E5E5),
      body: selectedIndex == 0 ? presentation() : registration(),
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(
              Icons.list_alt,
              size: 24,
            ),
            label: 'Обо мне',
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.alarm_add,
              size: 26.67,
            ),
            label: 'Записаться',
          ),
        ],
        selectedLabelStyle: TextStyle(
            fontSize: 16,
            fontWeight: FontWeight.w500,
            fontFamily: 'Montserrat-Medium.ttf'),
        unselectedLabelStyle: TextStyle(
            fontSize: 16,
            fontWeight: FontWeight.w500,
            fontFamily: 'Montserrat-Medium.ttf'),
        currentIndex: this.selectedIndex,
        selectedItemColor: Color(0xFF35D4BE),
        unselectedItemColor: Color(0xFF101B2B),
        onTap: (int index) {
          this.setState(() {
            this.selectedIndex = index;
          });
        },
      ),
    );
  }

  Widget presentation() {
    return ListView(
      children: [
        Image.asset(
          'assets/images/image_2021-08-20_13-46-41.png',
          fit: BoxFit.cover,
        ),
      ],
    );
  }

  Widget registration() {
    return ListView(children: [
      Container(
        margin: EdgeInsets.only(top: 24.0, left: 16.0),
        child: Text(
          'Запись на приём',
          style: TextStyle(
              fontSize: 24,
              color: Color(0xFF000000),
              fontWeight: FontWeight.w700,
              fontFamily: 'Montserrat-Bold.ttf'),
        ),
      ),
      Container(
        margin: EdgeInsets.only(left: 16.0, bottom: 8.0),
        child: Text(
          'Работаю онлайн или очно',
          style: TextStyle(
              fontSize: 12,
              color: Color(0xFF35D4BE),
              fontWeight: FontWeight.w500,
              fontFamily: 'Montserrat-Medium.ttf'),
        ),
      ),
      Row(
        children: [
          Expanded(
            child: Container(
                child: Image.asset(
              'assets/images/image_2021-08-20_13-50-16.png',
              fit: BoxFit.cover,
            )),
          ),
          Expanded(
            child: Container(
              child: Column(
                children: [
                  Container(
                    margin: EdgeInsets.only(left: 6.0),
                    width: double.infinity,
                    child: Text(
                      'Адрес и часы работы',
                      style: TextStyle(
                          fontSize: 12.0,
                          color: Color(0xFF000000),
                          fontWeight: FontWeight.w400,
                          fontFamily: 'Montserrat-Regular.ttf'),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 6.0),
                    width: double.infinity,
                    child: Text(
                      'Москва, ул.\nСадовническая, 78 стр. 5\nМетро Павелецкая\n\nПН-ПТ: 10:00 - 20:00\nСБ: 11:00 - 16:00\n',
                      style: TextStyle(
                          fontSize: 12,
                          color: Color(0xFF000000),
                          fontWeight: FontWeight.w500,
                          fontFamily: 'Montserrat-Medium.ttf'),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 6.0),
                    width: double.infinity,
                    child: Text(
                      'Нажмите, чтобы перейти',
                      style: TextStyle(
                          fontSize: 12.0,
                          color: Color(0xFF000000),
                          fontWeight: FontWeight.w400,
                          fontFamily: 'Montserrat-Regular.ttf'),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 6.0),
                    width: double.infinity,
                    child: Text(
                      '+7 917 515-18-05',
                      style: TextStyle(
                          fontSize: 12.0,
                          color: Color(0xFF000000),
                          fontWeight: FontWeight.w500,
                          fontFamily: 'Montserrat-Medium.ttf'),
                    ),
                  ),
                  Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Expanded(
                          child: OutlinedButton(
                            onPressed: () {},
                            child: Image.asset(
                                'assets/images/image_2021-08-20_13-34-04.png',
                                color: Color(0xFFFFFFFF)),
                            style: OutlinedButton.styleFrom(
                              backgroundColor: Color(0xFF48C95F),
                              shape: CircleBorder(),
                              minimumSize: Size(32, 32),
                            ),
                          ),
                        ),
                        Expanded(
                          child: OutlinedButton(
                            onPressed: () {},
                            child: Image.asset(
                                'assets/images/image_2021-08-20_13-27-24.png',
                                color: Color(0xFFFFFFFF)),
                            style: OutlinedButton.styleFrom(
                              backgroundColor: Color(0xFF7C509A),
                              shape: CircleBorder(),
                              minimumSize: Size(32, 32),
                            ),
                          ),
                        ),
                        Expanded(
                          child: OutlinedButton(
                            onPressed: () {},
                            child: Image.asset(
                                'assets/images/image_2021-08-20_13-39-33.png',
                                color: Color(0xFFFFFFFF)),
                            style: OutlinedButton.styleFrom(
                              backgroundColor: Color(0xFF2F89CE),
                              shape: CircleBorder(),
                              minimumSize: Size(32, 32),
                            ),
                          ),
                        ),
                        Expanded(
                          child: OutlinedButton(
                            onPressed: () {},
                            child: Image.asset(
                                'assets/images/image_2021-08-20_13-42-18.png',
                                color: Color(0xFFFFFFFF)),
                            style: OutlinedButton.styleFrom(
                              backgroundColor: Color(0xFF48C95F),
                              shape: CircleBorder(),
                              minimumSize: Size(32, 32),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
      Column(
        children: [
          Container(
            margin: EdgeInsets.only(top: 16.0, left: 19.0, right: 19.0),
            height: 54,
            child: TextField(
              decoration: InputDecoration(
                hintText: 'Имя',
                hintStyle: TextStyle(
                    fontSize: 16,
                    color: Color(0xFF101B2B).withOpacity(0.5),
                    fontWeight: FontWeight.w500,
                    fontFamily: 'Montserrat-Medium.ttf'),
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(8.0)),
                  borderSide: BorderSide(color: Color(0xFF35D4BE), width: 2.0),
                ),
                focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(8.0)),
                    borderSide:
                        BorderSide(color: Color(0xFF35D4BE), width: 2.0)),
              ),
              keyboardType: TextInputType.phone,
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 8.0, left: 19.0, right: 19.0),
            height: 54,
            child: TextField(
              decoration: InputDecoration(
                hintText: 'Почта',
                hintStyle: TextStyle(
                    fontSize: 16,
                    color: Color(0xFF101B2B).withOpacity(0.5),
                    fontWeight: FontWeight.w500,
                    fontFamily: 'Montserrat-Medium.ttf'),
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(8.0)),
                  borderSide: BorderSide(color: Color(0xFF35D4BE), width: 2.0),
                ),
                focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(8.0)),
                    borderSide:
                        BorderSide(color: Color(0xFF35D4BE), width: 2.0)),
              ),
              keyboardType: TextInputType.phone,
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 8.0, left: 19.0, right: 19.0),
            height: 54,
            child: TextField(
              decoration: InputDecoration(
                hintText: 'Телефон',
                hintStyle: TextStyle(
                    fontSize: 16,
                    color: Color(0xFF101B2B).withOpacity(0.5),
                    fontWeight: FontWeight.w500,
                    fontFamily: 'Montserrat-Medium.ttf'),
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(8.0)),
                  borderSide: BorderSide(color: Color(0xFF35D4BE), width: 2.0),
                ),
                focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(8.0)),
                    borderSide:
                        BorderSide(color: Color(0xFF35D4BE), width: 2.0)),
              ),
              keyboardType: TextInputType.phone,
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 8.0, left: 19.0, right: 19.0),
            height: 150,
            child: TextField(
              decoration: InputDecoration(
                hintText: 'Комментарий',
                hintStyle: TextStyle(
                    fontSize: 16,
                    color: Color(0xFF101B2B).withOpacity(0.5),
                    fontWeight: FontWeight.w500,
                    fontFamily: 'Montserrat-Medium.ttf'),
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(8.0)),
                  borderSide: BorderSide(color: Color(0xFF35D4BE), width: 2.0),
                ),
                focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(8.0)),
                    borderSide:
                        BorderSide(color: Color(0xFF35D4BE), width: 2.0)),
              ),
              maxLines: 9,
              keyboardType: TextInputType.phone,
            ),
          ),
        ],
      ),
      Container(
        margin: EdgeInsets.only(top: 32.0, left: 16.0, right: 16.0, bottom: 32),
        height: 52,
        width: double.infinity,
        child: ElevatedButton(
          onPressed: () {
            setState(() {
              return selectedIndex = 0;
            });
          },
          child: Text(
            'Записаться',
            style: TextStyle(
                fontSize: 16,
                color: Color(0xFFFEFEFE),
                fontWeight: FontWeight.w700,
                fontFamily: 'Montserrat-Bold.ttf'),
          ),
          style: ButtonStyle(
              backgroundColor:
                  MaterialStateProperty.all<Color>(Color(0xFF35D4BE))),
        ),
      ),
    ]);
  }
}
