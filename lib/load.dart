import 'package:flutter/material.dart';
import 'package:psyinfo/login.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.grey,
      ),
      home: Home(),
    );
  }
}

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  void initState() {
    super.initState();
    Future.delayed(const Duration(seconds: 3), () {
      Navigator.of(context).pushReplacement(
        MaterialPageRoute(
          builder: (_) => Login(),
        ),
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFF35D4BE),
      body: Align(
        alignment: Alignment.center,
        child: Container(
          height: 194,
          width: 244,
          child: Column(
            children: [
              Align(
                alignment: Alignment.centerRight,
                child: Text(
                  'Psy',
                  style: TextStyle(
                    fontSize: 128,
                    color: Color(0xFFFEFEFE),
                    fontWeight: FontWeight.w800,
                    fontFamily: 'Montserrat-ExtraBold.ttf',
                  ),
                ),
              ),
              Align(
                alignment: Alignment.centerRight,
                child: Text(
                  'info',
                  style: TextStyle(
                    fontSize: 48,
                    height: 0.68,
                    color: Color(0xFF000000),
                    fontWeight: FontWeight.w800,
                    fontFamily: 'Montserrat-ExtraBold.ttf',
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
